//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/lazysizes/lazysizes.js

//= template/scripts.js
//= template/jquery.themepunch.tools.min.js
//= template/jquery.themepunch.revolution.min.js

//= template/revolution.extension.actions.min.js
//= template/revolution.extension.carousel.min.js
//= template/revolution.extension.kenburn.min.js
//= template/revolution.extension.layeranimation.min.js
//= template/revolution.extension.migration.min.js
//= template/revolution.extension.navigation.min.js
//= template/revolution.extension.parallax.min.js
//= template/revolution.extension.slideanims.min.js
//= template/revolution.extension.video.min.js

$(document).ready(function () {

	function moveMinicart () {
		var scroll_wrapper = $('#cart-scroll');
		var scroll = scroll_wrapper.children();
		var desktop_wrapper = $('#cart-desktop');
		var desktop = desktop_wrapper.children();
		var mobile_wrapper = $('#cart-mobile');
		var mobile = mobile_wrapper.children();

		if ( window.innerWidth < 991 ) {
			// Вставить в мобилку
			if (mobile.lenght) return false;
				mobile_wrapper.append(desktop.detach());
		} else {
			if (desktop.lenght) return false;
			desktop_wrapper.append( mobile.detach() );
		}

		if ( $(window).scrollTop() > 100 ) {
			// Вставить в декстом с скролом
			if (scroll.lenght) {
				return false;
			}
			scroll_wrapper.append(desktop.detach());
		} else {
			// Вставить в декстом без скрола
			if (desktop.lenght) {
				return false;
			}
			desktop_wrapper.append( scroll.detach() );
		}
	}
	$(window).scroll(function () {
		moveMinicart();
	});
	$(window).resize(function () {
		moveMinicart();
	});
	moveMinicart ();





	$(".dropdown-menu.dropdown-menu-right").on("click", function(e){
		e.stopPropagation();
	});

});
